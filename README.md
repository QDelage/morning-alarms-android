# Morning Alarms

Android app to create my 6 morning-routine alarm of the morning considering the hour of my first morning lesson.

## Features

* Creation of 6 morning alarms (2 hours before the lesson, 2 mins later, 3 later, 5 later, one hour before the lesson, twenty min before the lesson).

### Upcoming features

The order these will be implemented, or even IF they will be implemented is not known yet.

* Configuration of the number, relative time and name of the alarms.
* Removing all the alarms created by the app (and only them).
* Other languages
* Apply to the F-Droid repository

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Android SDK (27 min for my settings, change at your own risks).

### Installing

A step by step series of examples that tell you how to get a development env running

Clone the repository

```
git clone git@gitlab.com:QDelage/morning-alarms-android.git
```

Compile the apk using Gradle in the new folder (or Android Studio, or any compatible GUI)
```
./gradlew build
```

The unsigned apk should be in the app/build/outputs/apk/release/ folder.

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Gradle](https://gradle.org/) - Dependency and build Management

## Authors

* **Quentin Delage** - *Initial work* - [QDelage](https://gitlab.com/QDelage)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE) file for details

## Acknowledgments

* Hat tip to [AAnkit](https://stackoverflow.com/a/11493437/11283971) for the alarm creation.
