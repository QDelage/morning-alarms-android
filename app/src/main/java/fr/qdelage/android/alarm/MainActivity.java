package fr.qdelage.android.alarm;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
public class MainActivity extends AppCompatActivity {
    TimePicker mTimePicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTimePicker = (TimePicker) findViewById(R.id.time);

        mTimePicker.setIs24HourView(true);

    }


    public void launch(View v) {
        if (mTimePicker.validateInput()) {
            Intent intentReveil = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentReveil.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour() - 2) % 24);
            intentReveil.putExtra(AlarmClock.EXTRA_MINUTES, mTimePicker.getMinute());
            intentReveil.putExtra(AlarmClock.EXTRA_MESSAGE, "Heure de réveil");
            intentReveil.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intentReveilRappel = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentReveilRappel.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour() - 2) % 24);
            intentReveilRappel.putExtra(AlarmClock.EXTRA_MINUTES, (mTimePicker.getMinute() + 2) % 60);
            intentReveilRappel.putExtra(AlarmClock.EXTRA_MESSAGE, "Rappel réveil");
            intentReveilRappel.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intentRappelBis = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentRappelBis.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour() - 2) % 24);
            intentRappelBis.putExtra(AlarmClock.EXTRA_MINUTES, (mTimePicker.getMinute() + 5) % 60);
            intentRappelBis.putExtra(AlarmClock.EXTRA_MESSAGE, "Rappel réveil bis");
            intentRappelBis.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intentRappelDebout = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentRappelDebout.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour() - 2) % 24);
            intentRappelDebout.putExtra(AlarmClock.EXTRA_MINUTES, (mTimePicker.getMinute() + 10) % 60);
            intentRappelDebout.putExtra(AlarmClock.EXTRA_MESSAGE, "Rappel réveil debout");
            intentRappelDebout.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intentRappelSDB = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentRappelSDB.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour() - 1) % 24);
            intentRappelSDB.putExtra(AlarmClock.EXTRA_MINUTES, (mTimePicker.getMinute()) % 60);
            intentRappelSDB.putExtra(AlarmClock.EXTRA_MESSAGE, "Rappel réveil SDB");
            intentRappelSDB.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intentDepart = new Intent(AlarmClock.ACTION_SET_ALARM);
            intentDepart.putExtra(AlarmClock.EXTRA_HOUR, (mTimePicker.getHour()) % 24);
            intentDepart.putExtra(AlarmClock.EXTRA_MINUTES, (mTimePicker.getMinute() - 20) % 60);
            intentDepart.putExtra(AlarmClock.EXTRA_MESSAGE, "Départ cours");
//            intentDepart.putExtra(AlarmClock.EXTRA_SKIP_UI, true);

            Intent intents[] = {intentReveil, intentReveilRappel, intentRappelBis, intentRappelDebout, intentRappelSDB, intentDepart};

            startActivityForResult(intentReveil, 0);
            startActivityForResult(intentReveilRappel, 0);
            startActivityForResult(intentRappelBis, 0);
            startActivityForResult(intentRappelDebout, 0);
            startActivityForResult(intentRappelSDB, 0);
            startActivityForResult(intentDepart, 0);

        } else {

        }


    }
}